# Onioname


## Introduction

**Onioname** is a very basic small-scale bottom-up DNS-like protocol backed up
by the Tor network.  It allows you to share your public IP address(es) with
people you know and get assigned a specific `.oni` domain name on their hosts.

**Onioname** takes advantage of the static nature of Tor's Hidden Services
hostnames to cope with system unavailablities.  It also takes advantage of the
*Client Authorization* feature in Tor to make authentication much simpler.

*Note:* Currently, **Onioname** is under developement (alpha) and only runs on
Debian hosts.


## Setup

In **Onioname**, there are two kinds of nodes:

- *Push* nodes: They register their public IP with a `.oni` domain.
- *Listen* nodes: They receive IP addresses from active nodes

You can actually run both kinds of nodes in the same host.

- To setup a *Push* node, read the [push/GUIDE.md](push/GUIDE.md) document.
- To setup a *Listen* node, read the [listen/GUIDE.md](listen/GUIDE.md) document.


## License

> Copyright (C) 2021 Karam Assany (karam.assany@disroot.org)
>
> Onioname is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> Onioname is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with Onioname.  If not, see <https://www.gnu.org/licenses/>.
