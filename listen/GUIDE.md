## onioname.listen


### Dependencies

1. POSIX-complaint shell
1. Debian
1. Tor
1. Python 3
1. systemd


### Installation Guide

1. Run `sudo ./install.sh`
1. Get your address from `sudo cat /var/lib/tor/onioname/hostname`
1. Send it to your friend and wait for response
1. Receive the public key from your friend
1. Move it to its appropriate place: `sudo install -m644 -o debian-tor -g debian-tor <public_key_file> /var/lib/tor/onioname/authorized_clients/`
1. Edit `/etc/onioname.listen.conf` to suit your preferences
1. Enable and start the Onioname listening service: `sudo systemctl enable --now onioname.service`
1. Now wait till you receive a push from your friend.  Read your `/etc/hosts` to see if it's updated correctly
1. DONE :)


### Removal Guide

1. Run `sudo ./remove.sh` and say `y`
1. Delete the Onioname-related entries from your `/etc/hosts`
1. DONE :)
