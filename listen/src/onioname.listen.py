#!/usr/bin/env python3
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# This file is part of Onioname.
#
# Onioname is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onioname is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onioname.  If not, see <https://www.gnu.org/licenses/>.
#



import os, configparser, socket


conf = configparser.ConfigParser()
conf.read("/etc/onioname.listen.conf")

onioname_port = conf.getint("onioname", "port")
onioname_service = conf.get("onioname", "service_dir")

enable_log = conf.getboolean("system", "enable_log")
if enable_log: log_file = conf.get("system", "log_file")


def log(msg):

    if enable_log:
        with open(log_file, "a") as f:
            print("LOG: " + msg, file=f)


log ("Read configuration from /etc/onioname.listen.conf")

domain = ''

log("Obtaining domain name")

for f in os.listdir(onioname_service + "/authorized_clients"):
    if f.endswith(".auth"):
        domain = f[:-5] + ".oni"
        break

if not domain:
    raise Exception("No domain available.  Please copy the public key file")

log("Opening a listening socket")

ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
ss.bind(("127.0.0.1", onioname_port))
ss.listen()

while True:

    try:
        
        log("Waiting for a request")
        
        cc, ca = ss.accept()

        log("Request arrived")

        try:
            if ca[0] != "127.0.0.1":
                raise Exception("Non-local request.  Watchout your firewall")

            req = cc.recv(1024).decode().split()

            if req[0] != "ONIONAME":
                raise Exception("Non-ONIONAME request")

            if len(req) < 2:
                raise Exception("Inefficient arguments for a request")

        except Exception as e:
            log("Invalid request: " + repr(e.args[0]))
            cc.sendall("ONIONAME FAILED".encode())
            cc.close()
            continue

        log("Accepted request")
        cc.sendall("ONIONAME OK".encode())
        cc.close()

        log("Generating the new /etc/hosts entry")
        updated_entry = "\t".join([req[1], domain] +
                [(alias + "." + domain) for alias in req[2:]]) + "\n"

        log("Updating /etc/hosts")
        with open('/etc/hosts', 'r+') as f:

            lines = f.readlines()
            entry_num = -1

            for num, line in enumerate(lines):

                if line.strip() and not line.startswith('#'):
                    if line.split()[1] == domain:
                        entry_num = num
                        break

            if entry_num == -1:
                log("Appending to /etc/hosts")
                lines.append(updated_entry)
            else:
                log("Modifying /etc/hosts")
                lines[entry_num] = updated_entry

            log("Saving changes")
            f.seek(0)
            f.truncate()
            f.writelines(lines)

    except BaseException as e:

        log("Fatal failure: " + repr(e.args))
        break
    
log("Closing the listening socket")
ss.close()

log("Exiting")
exit(1)
