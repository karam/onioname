#!/bin/sh -e
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# This file is part of Onioname.
#
# Onioname is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onioname is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onioname.  If not, see <https://www.gnu.org/licenses/>.
#



log() {
	echo "-> $1" >&2
}



log "Checking if this script is running as root"

test $(id -u) -eq 0 || {
	echo "Please run this script as root" >&2
	exit 1
}


log "Checking if this script is running on a Debian system"

test "$(cat /etc/os-release | grep '^ID=' | cut -d'=' -f2)" = "debian" || {
	echo "Onioname currently only support Debian" >&2
	exit 1
}


log "Checking if Tor is installed"

dpkg -s tor > /dev/null


log "Installing onioname.listen"

install src/onioname.listen.py /usr/local/bin/onioname.listen


log "Installing onioname.listen.conf"

install -m644 src/onioname.listen.conf /etc/onioname.listen.conf


log "Registering a new hidden service in /etc/tor/torrc"

{
	echo "### This was automatically appended by the Onioname installer"
	echo "### BEGIN"
	echo "HiddenServiceDir /var/lib/tor/onioname"
	echo "HiddenServicePort 977 127.0.0.1:977"
	echo "### END"
	echo
} >> /etc/tor/torrc


log "Reloading Tor configuration"

pkill -HUP tor


log "Installing the systemd unit"

install -m644 src/onioname.service /etc/systemd/system/onioname.service


log "DONE"
