#!/bin/sh -e
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# This file is part of Onioname.
#
# Onioname is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onioname is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onioname.  If not, see <https://www.gnu.org/licenses/>.
#



log() {
	echo "-> $1" >&2
}



log "Checking if this script is running as root"

test $(id -u) -eq 0 || {
	echo "Please run this script as root" >&2
	exit 1
}


log "Checking if an onion address is given"

test $# -eq 1 || {
	echo "Please give the onion address" >&2
	exit 1
}


log "Checking if the script is running on a Debian system"

test "$(cat /etc/os-release | grep '^ID=' | cut -d'=' -f2)" = "debian" || {
    echo "Onioname currently only support Debian" >&2
    exit 1
}


log "Checking if Tor is installed"

dpkg -s tor > /dev/null


log "Checking if Python3's 'socks' module is installed"

python3 -m socks || {
	echo "Not installed.  Please install it with:" >&2
	echo "  sudo apt install python3-socks" >&2
	exit 1
}


log "Installing onioname.push"

install src/onioname.push.py /usr/local/bin/onioname.push


log "Installing onioname.push.conf"

install -m644 src/onioname.push.conf /etc/onioname.push.conf


log "Installing the NetworkManager hook script"

install src/99-onioname.sh /etc/NetworkManager/dispatcher.d/99-onioname


log "Installing the cronjob script"

install src/onioname.cron.sh /etc/cron.hourly/onioname


log "Checking if Client Authorization is enabled for Tor"

if cat /etc/tor/torrc | sed -e '/^[ \t]*#/d' | grep -q ClientOnionAuthDir

then
	
	log "NOTE: It's enabled!"

else
	
	log "NOTE: Not enabled.  Will enable it"

	install -d -o debian-tor -g debian-tor /var/lib/tor/client_auth

	echo "ClientOnionAuthDir /var/lib/tor/client_auth" >> /etc/tor/torrc

	
	log "Reloading Tor configuration"

	pkill -HUP tor

fi

log "Generating private key"

TMP="$(mktemp)"

chmod 0600 "$TMP"

openssl genpkey -algorithm x25519 > "$TMP"

echo "$1:descriptor:x25519:$(cat "$TMP" | grep -v "PRIVATE KEY" | base64 -d | \
	tail --bytes=32 | base32 | sed 's/=//g')" > \
	"$(cat /etc/tor/torrc | sed -e '/^[ \t]*#/d' | grep ClientOnionAuthDir | \
	head -1 | cut -d' ' -f2)/onioname.auth_private"

echo "descriptor:x25519:$(openssl pkey -in "$TMP" -pubout | \
	grep -v "PUBLIC KEY" | base64 -d | tail --bytes=32 | base32 | \
	sed 's/=//g')" > public_key.auth

chown debian-tor:debian-tor "$(cat /etc/tor/torrc | sed -e '/^[ \t]*#/d' | \
	grep ClientOnionAuthDir | head -1 | cut -d' ' -f2)/onioname.auth_private"

chmod 0644 public_key.auth


log "NOTE: Public key was saved in ./public_key.auth"


log "DONE"
