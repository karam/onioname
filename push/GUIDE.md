## onioname.push


### Dependencies

1. POSIX-complaint shell
1. Debian
1. Tor
1. Python 3
1. `socks` module for Python
1. NetworkManager
1. cron
1. OpenSSL


### Installation Guide

1. Receive the address of your friend
1. Run `sudo ./install.sh <onion_address>`
1. You'll get your private and public key generated
1. Rename the key into your preferred domain (without the ".oni" part): `mv public_key.auth <YOUR_DOMAIN>.auth`
1. Send the public key to your friend
1. Edit /etc/onioname.push.conf to suit your preferences
1. Now manually send your friend a push signal: `sudo onioname.push`
1. If it went successful, worry no more.  Your host will send a push signal whenever a new connection occurs, as well as hourly (as a cronjob).
1. DONE :)


### Removal Guide

1. Run `sudo ./remove.sh` and say `y`
1. DONE :)
