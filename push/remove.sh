#!/bin/sh -e
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# This file is part of Onioname.
#
# Onioname is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onioname is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onioname.  If not, see <https://www.gnu.org/licenses/>.
#



log() {
	echo "-> $1" >&2
}



log "Checking if this script is running as root"

test $(id -u) -eq 0 || {
	echo "Please run this script as root" >&2
	exit 1
}


log "This is going to remove Onioname along with its data."

read -p"Are you sure? [y/N] " REP

test "$REP" = "y"


log "Deleting files"

rm -f /usr/local/bin/onioname.push

rm -f /etc/NetworkManager/dispatcher.d/99-onioname
rm -f /etc/cron.hourly/onioname

grep '^status_file' /etc/onioname.push.conf | cut -d'=' -f2 | xargs rm -f
grep '^key_file' /etc/onioname.push.conf | cut -d'=' -f2 | xargs rm -f

rm -f /etc/onioname.push.conf


log "DONE"
