#!/usr/bin/env python3
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# This file is part of Onioname.
#
# Onioname is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onioname is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onioname.  If not, see <https://www.gnu.org/licenses/>.
#



import os, configparser, json, socket, dns.resolver
import socks


conf = configparser.ConfigParser()
conf.read("/etc/onioname.push.conf")

onioname_port = conf.getint("onioname", "port")
onioname_subs = conf.get("onioname", "subdomains")
onioname_key = conf.get("onioname", "key_file")

enable_log = conf.getboolean("system", "enable_log")
status_file = conf.get("system", "status_file")

resolve_server = conf.get("resolver", "server")
resolve_address = conf.get("resolver", "address")

torsocks_address = conf.get("tor_proxy", "address")
torsocks_port = conf.getint("tor_proxy", "port")


def log(msg):

    if enable_log:
        print("LOG: " + msg)


log("Read configuration from /etc/onioname.push.conf")

log("Obtaining target domain name")

with open(onioname_key) as f:
    target = f.readlines()[0].strip().split(":")[0]

if not target.endswith(".onion"):
    raise Exception("Improper private key file")

log("Loading previous status")

status = {"ip": "0.0.0.0", "passed": True}

if os.path.exists(status_file):
    try:
        with open(status_file) as f:
            status = json.load(f)
            log("Loaded previous status")
    except json.JSONDecodeError:
        log("Invalid status file.  Created a new one")
else:
    log("No previous status.  Created a new one")

log("Obtaining public IP from resolver")

resolver = dns.resolver.Resolver()
resolver.nameservers=[socket.gethostbyname(resolve_server)]

new_ip = resolver.resolve(resolve_address)[0].to_text()

log("Obtained public IP successfully")

if new_ip == status["ip"]:
    if status["passed"]:
        log("Same old IP with successful status.  Aborting")
        exit(0)

log("New IP detected")

status["ip"] = new_ip

log("Connecting to target through Tor's SOCKS5 proxy")

socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, torsocks_address, torsocks_port,
        True)

with socks.socksocket() as ss:
    ss.connect((target, onioname_port))
    log("Connected.  Sending request")
    ss.sendall("ONIONAME {}{}".format(new_ip,
            (" " + onioname_subs) if onioname_subs else ""
        ).encode())
    resp = ss.recv(1024).decode()

status["passed"] = resp == "ONIONAME OK"

if status["passed"]:
    log("Request was successful")
else:
    log("Request has failed")

with open(status_file, "w") as f:
    log("Writing status to disk")
    json.dump(status, f)

log("Exiting")
